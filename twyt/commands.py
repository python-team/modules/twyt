# Copyright (c) 2007-2009 Andrew Price
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""Command line parsing and processing for twyt"""

import sys
import codecs
import getpass
import twitter
import config
import data
from info import VERSION
from optparse import OptionParser, make_option

class Commands:

	"""Provides a convenient interface to the twyt commands"""

	def __init__(self):

		if not sys.stdout.isatty():
			sys.stdout = codecs.getwriter("UTF-8")(sys.stdout)

		self.commands = {}
		self.parser = OptionParser(usage="%prog COMMAND [options]",
				version=VERSION)

		self.config = config.Config()
		self.twitter = twitter.Twitter()
		self.twitter.set_user_agent("Twyt")
	
	def register_command(self, command):
		self.commands[str(command)] = command

	def __lookup_command(self, command):
		try:
			return self.commands[command]
		except KeyError, k:
			return DefaultCommand()

	def __parse_command(self, command, argv):
		self.parser.prog = "%s %s" %\
			(self.parser.get_prog_name(), str(command))
		self.parser.set_usage(command.get_usage())
		optlst = command.get_options()
		for o in optlst:
			self.parser.add_option(o)
		self.parser.set_description(command.get_help())

		if str(command) != "":
			argv = argv[1:]

		return self.parser.parse_args(argv)

	def process_argv(self, argv):
		try:
			command = self.__lookup_command(argv[1])
		except IndexError, i:
			# This exits
			self.parser.error("No command specified. Try --help")

		options, args = self.__parse_command(command, argv[1:])

		command.do_command(options, args, self)

		self.config.dirty_writeback()
	
	def output(self, str):
		enc = sys.stdout.encoding
		if enc is None:
			print unicode(str)
		else:
			print unicode(str).encode(enc, "replace")


class Command:
	def __init__(self):
		self.options = []

	def get_options(self):
		return self.options

class DefaultCommand(Command):
	def __str__(self):
		return ""

	def get_options(self):
		self.options.append(make_option("-c","--commands",
				action="store_true",
				help="Lists available commands"))
		return self.options

	def get_usage(self):
		return "%prog COMMAND [options] [args]"

	def get_help(self):
		return "Twyt is a command line Twitter client written using, "\
			"and specifically to test, the python-twyt library.\n"\
			"\nSee --commands for possible values of COMMAND"

	def do_command(self, options, args, base):
		if options.commands:
			base.parser.print_usage()
			base.output("Available commands:")
			keys = base.commands.keys()
			keys.sort()
			for k in keys:
				base.output(" %12s %s" %\
					(k, base.commands[k].get_help()))
			base.output("")
			base.output("For command-specific help, use "\
				"%sCOMMAND --help" % base.parser.get_prog_name())
		else:
			base.parser.error("Command not found. Try --commands")


class CommandWithAuth:

	def __init__(self):
		self.options = 	[
			make_option("-u", "--user", metavar="USER",
			help="Specifies USER as your username. USER is "
			"then stored as your default username."),
			make_option("-p", "--pass", metavar="PASS",
			dest="password",
			help="Specifies PASS as your password.")
		]

	def get_options(self):
		return self.options

        def passwdprompt(self, usr):
		pwd = ""
		for _ in range(3):
			pwd = getpass.getpass("Enter %s's Twitter password: " % usr)
			if pwd != "":
				break

		return pwd

	def get_auth(self, options, base):
		user = options.user or base.config.get_default_account()
		pwd = options.password or base.config.get_password(user)

		if not user:
			# This exits
			base.parser.error('No username specified.')
		
		if not pwd:
			pwd = self.passwdprompt(user)

		return (user, pwd)


	def callwithauth(self, func, options, data, base, user, pwd):

		if not pwd:
			base.parser.error('No password specified.')
		
		base.twitter.set_auth(user, pwd)

		answer = ""

		try:
			answer = func(*data)
		except twitter.TwitterAuthException:
			base.parser.error('Bad username/password')

		return answer


class TweetCommand(CommandWithAuth):
	def __str__(self):
		return "tweet"

	def get_options(self):
		self.options.append(make_option("-r", "--replyto",
			metavar="ID", default=-1, type="int",
			help="Explicitly sets the ID of the status message to "
			"reply to."))
		return self.options

	def get_usage(self):
		return "%prog [options] message..."

	def get_help(self):
		return "Updates the authenticating user's Twitter status"

	def _do_tweet(self, options, base, msg):
		if not msg:
			return False

		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.status_update, options,
				[msg, options.replyto], base, user, pwd)

		result = data.Status()
		result.load_json(answer)

		base.output(result)
		return True
	
	def _get_tweets(self, args, base):
		if not sys.stdin.isatty():
			line = sys.stdin.readline()
			while line:
				if line.endswith("\n"):
					line = line[:-1]
				if line:
					yield line
				line = sys.stdin.readline()
		elif len(args) > 0:
			yield " ".join(args)
		else:
			base.parser.error("No message specified.")

	def do_command(self, options, args, base):
		for tweet in self._get_tweets(args, base):
			self._do_tweet(options, base, tweet)


class SingCommand(TweetCommand):
	def __str__(self):
		return "sing"

	def get_help(self):
		return "Similar to 'tweet', wraps the status in musical notes"

	def do_command(self, options, args, base):
		note = '\xe2\x99\xab'
		for tweet in TweetCommand._get_tweets(self, args, base):
			TweetCommand._do_tweet(self, options, base,
					" ".join([note, tweet, note]))
		

class DirectCommand(CommandWithAuth):
	def __str__(self):
		return "direct"

	def get_options(self):
		return self.options

	def get_usage(self):
		return "%prog [options] RECIPIENT MESSAGE..."

	def get_help(self):
		return "Sends a direct message to another user"

	def do_command(self, options, args, base):
		if len(args) < 1:
			base.parser.error("No recipient specified.")

		if len(args) < 2:
			base.parser.error("No message specified.")

		touser = args[0]
		msg = " ".join(args[1:])
		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth( base.twitter.direct_new, options,
				[touser, msg], base, user, pwd)

		result = data.DirectMsg()
		result.load_json(answer)
		
		if not result.id:
			raise twitter.TwitterException(
					"Sending direct message	failed.")

		base.output(result)


class DirectTLCommand(CommandWithAuth):
	def __str__(self):
		return "directtl"

	def get_options(self):
		self.options.append(make_option("-s", "--since",
			metavar="SINCE",
			help="The date or ID of a message to list direct "
			"messages from."))
		self.options.append(make_option("-P", "--page",
			metavar="PAGE", default=1, type="int",
			help="Lists the PAGEth page of direct messages "
			"(default 1)"))

		return self.options

	def get_usage(self):
		return "%prog [options]"

	def get_help(self):
		return "Prints the 20 last direct messages sent to you"

	def do_command(self, options, args, base):
		since = ""
		since_id = ""
		# Since can be an ID or date
		if options.since:
			try:
				since_id = int(options.since)
			except ValueError:
				since = options.since
	
		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.direct_messages,
				options, [since, since_id, options.page], base,
				user, pwd)

		results = data.DirectList(answer)
		
		for result in results:
			base.output(result)


class DirectSentCommand(CommandWithAuth):
	def __str__(self):
		return "directsent"

	def get_options(self):
		self.options.append(make_option("-s", "--since",
			metavar="SINCE",
			help="The date or ID of a message to list direct "
			"messages from."))
		self.options.append(make_option("-P", "--page",
			metavar="PAGE", default=1, type="int",
			help="Lists the PAGEth page of direct messages "
			"(default 1)"))

		return self.options

	def get_usage(self):
		return "%prog [options]"

	def get_help(self):
		return "Prints the 20 last direct messages sent by you"

	def do_command(self, options, args, base):
		since = ""
		since_id = ""
		# since can be an id or a date
		if options.since:
			try:
				since_id = int(options.since)
			except ValueError:
				since = options.since

		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.direct_sent, options,
				[since, since_id, options.page], base, user,
				pwd)

		results = data.DirectList(answer)
		
		for result in results:
			base.output(result)


class DirectDelCommand(CommandWithAuth):
	def __str__(self):
		return "directdel"

	def get_options(self):
		return self.options

	def get_usage(self):
		return "%prog [options] ID"

	def get_help(self):
		return "Delete a direct message which was sent to you"

	def do_command(self, options, args, base):

		sid = -1
		if len(args) > 0:
			try:
				sid = int(args[0])
			except ValueError:
				pass

		if sid < 0:
			base.parser.error("Invalid ID specified")

		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.direct_destroy, options,
				[sid], base, user, pwd)
		if answer:
			result = data.DirectMsg()
			result.load_json(answer)

			base.output(u'Deleted: ' + unicode(result))
		else:
			base.output("No messages deleted. Are you authorized?")
	

class PublicTLCommand(Command):
	def __str__(self):
		return "publictl"
	
	def get_options(self):
		return self.options

	def get_usage(self):
		return "%prog [options]"

	def get_help(self):
		return "Shows the 20 most recent statuses in Twitter's "\
			"public timeline"

	def do_command(self, options, args, base):

		answer = base.twitter.status_public_timeline()

		try:
			results = data.StatusList(answer)
		except Exception, e:
			raise twitter.TwitterException(e)

		for result in results:
			base.output(result)

		if len(results) == 0:
			base.output("(Nothing new)")


class FriendsTLCommand(CommandWithAuth):
	def __str__(self):
		return "friendstl"

	def get_options(self):
		self.options.append(make_option("-s", "--since",
			metavar="SINCE",
			help="The date or ID of a message to list status "
			"messages from."))
		self.options.append(make_option("-P", "--page",
			metavar="PAGE", type="int",
			help="Lists the PAGEth page of status updates "))
		self.options.append(make_option("-n", "--count",
			metavar="COUNT",
			help="The number of statuses to retrieve (max 200)"))
		self.options.append(make_option("-q", "--quiet",
			default=False, action="store_true",
			help="Don't print the 'Nothing new' message."))
		return self.options

	def get_usage(self):
		return "%prog [options]"

	def get_help(self):
		return "Returns 20 most recent statuses in your friends "\
				"timeline"

	def do_command(self, options, args, base):

		user, pwd = self.get_auth(options, base)

		since_id = None
		since = ""
		if options.since:
			try:
				since_id = int(options.since)
			except ValueError:
				since = options.since

		catchup = False
		if not (since or since_id or options.page):
			since_id = base.config.get_stamp(self, user)
			catchup = True

		answer = self.callwithauth(base.twitter.status_friends_timeline,
				options,
				[since, since_id, options.page, options.count],
				base, user, pwd)

		try:
			results = data.StatusList(answer)
		except Exception, e:
			raise twitter.TwitterException(e)

		if since:
			base.output("Since %s:" % since)
		for result in results:
			base.config.name_cache_add(result.user.screen_name)
			base.output(result)

		if len(results) > 0 and catchup:
			base.config.set_stamp('friendstl', user,
					results.get_last_id())
		elif catchup and not options.quiet:
			base.output("(Nothing new)")


class UserTLCommand(CommandWithAuth):
	def __str__(self):
		return "usertl"

	def get_options(self):
		self.options.append(make_option("-s", "--since",
			metavar="SINCE",
			help="The date or ID to list statuses from."))
		self.options.append(make_option("-P", "--page",
			metavar="PAGE", type="int",
			help="Lists the PAGEth page of status updates "))
		self.options.append(make_option("-c", "--count",
			metavar="COUNT", default=20, type="int",
			help="The number of statuses to show, max 20"))
		return self.options

	def get_usage(self):
		return "%prog [options] [USERNAME]"

	def get_help(self):
		return "Show your timeline, or USERNAME's timeline"

	def do_command(self, options, args, base):
		count = 20
		if options.count > 0:
			count = options.count
		
		ident = ""
		if len(args) > 0:
			ident = args[0]

		since_id = None
		since = ""
		if options.since:
			try:
				since_id = int(options.since)
			except ValueError:
				since = options.since

		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.status_user_timeline,
				options, [ident, count, since, since_id,
					options.page], base, user, pwd)
		try:
			results = data.StatusList(answer)
		except Exception, e:
			raise twitter.TwitterException(e)

		for result in results:
			base.output(result)


class ShowCommand(CommandWithAuth):
	def __str__(self):
		return "show"

	def get_options(self):
		return self.options

	def get_usage(self):
		return "%prog ID"

	def get_help(self):
		return "Show a single status message by ID"

	def do_command(self, options, args, base):
		ident = ""
		if len(args) > 0:
			ident = args[0]
		else:
			base.parser.error("No ID specified.")

		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.status_show, options,
				[ident], base, user, pwd)

		if answer:
			result = data.Status()
			result.load_json(answer)
			
			base.output(result)


class RepliesCommand(CommandWithAuth):
	def __str__(self):
		return "replies"

	def get_options(self):
		self.options.append(make_option("-s", "--since",
			metavar="SINCE",
			help="The date or ID to list replies from."))
		self.options.append(make_option("-P", "--page", metavar="PAGE",
			type="int", default=1,
			help="Shows the PAGEth page of replies."))
		return self.options

	def get_usage(self):
		return "%prog [options]"

	def get_help(self):
		return "Lists statuses which are replies to you (statuses with "\
			"@yourusername in them)"

	def do_command(self, options, args, base):
		user, pwd = self.get_auth(options, base)
		since_id = None
		since = ""
		if options.since:
			try:
				since_id = int(options.since)
			except ValueError:
				since = options.since

		answer = self.callwithauth(base.twitter.status_replies, options,
				[options.page, since, since_id], base, user,
				pwd)

		if answer:
			try:
				results = data.StatusList(answer)
			except Exception, e:
				raise twitter.TwitterException(e)

			for result in results:
				 base.output(result)


class DeleteCommand(CommandWithAuth):
	def __str__(self):
		return "delete"

	def get_options(self):
		return self.options

	def get_usage(self):
		return "%prog [options] ID"

	def get_help(self):
		return "Deletes a tweet by ID"

	def do_command(self, options, args, base):
		sid = -1
		if len(args) > 0:
			try:
				sid = int(args[0])
			except ValueError:
				pass

		if sid < 0:
			base.parser.error("Invalid ID specified")

		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.status_destroy, options,
				[sid], base, user, pwd)
		
		if answer:
			result = data.Status()
			result.load_json(answer)

			base.output(u'Deleted: ' + unicode(result))
		else:
			base.output('No messages deleted. Are you authorised?')


class BlockCommand(CommandWithAuth):
	def __str__(self):
		return "block"

	def get_options(self):
		return self.options

	def get_usage(self):
		return "%prog [options] ID"

	def get_help(self):
		return "Blocks a user specified by ID (numerical ID or"\
				" screen name)"

	def do_command(self, options, args, base):
		ident = ""
		if len(args) <= 0:
			base.parser.error("ID not specified")

		ident = args[0]

		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.block_create, options,
				[ident], base, user, pwd)
		if answer:
			base.output("User ID %s blocked." % str(ident))


class UnblockCommand(CommandWithAuth):
	def __str__(self):
		return "unblock"

	def get_options(self):
		return self.options

	def get_usage(self):
		return "%prog [options] ID"

	def get_help(self):
		return "Unblocks a user specified by ID (numerical ID or"\
				" screen name)"

	def do_command(self, options, args, base):
		ident = ""
		if len(args) <= 0:
			base.parser.error("ID not specified")

		ident = args[0]

		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.block_destroy, options,
				[ident], base, user, pwd)
		if answer:
			base.output("User ID %s unblocked." % str(ident))

class UserCommand(Command):
	def __str__(self):
		return "user"

	def get_options(self):
		self.options.append(
			make_option("-u", "--user", metavar="USER",
			default="",
			help="Specify a Twitter username."))
		self.options.append(
			make_option("-p", "--pass", metavar="PASS",
			default="", dest="password",
			help="Specify a password."))
		self.options.append(make_option("-l", "--list",
			default=False, action="store_true",
			help="Shows all stored usernames."))
		self.options.append(make_option("-d", "--default",
			default=False, action="store_true",
			help="Set the given username as default."))
		return self.options

	def get_usage(self):
		return "%prog [options] (set|unset)"

	def get_help(self):
		return "Get and set Twyt user options, e.g. remembered "\
			"passwords and Twitter usernames"

	def __print_accounts(self, accs=[], default=""):
		for i,j in accs:
			if j:
				p = "password saved"
			else:
				p = "no password"
			if i == default:
				p += ", default"
			print("%15s (%s)" % (i, p))

	def __set_options(self, options, base):
		if options.password and options.user:
			base.config.set_password(options.user, options.password)
			if options.default:
				base.config.set_default(options.user)
		elif options.user:
			newacc = False
			if not base.config.have_account(options.user):
				base.config.add_account(options.user)
				newacc = True
			if options.default:
				base.config.set_default_account(options.user)
			if not options.default or newacc:
				base.config.set_password(options.user,
				  getpass.getpass(
				    "Enter %s's Twitter password: " %\
						    options.user))

	def __unset_options(self, options, base):
		if options.user:
			base.config.remove_account(options.user)

	def do_command(self, options, args, base):
		d = base.config.get_default_account()
		if options.list and options.user:
			a = base.config.get_account(options.user)
			if len(a) > 0:
				self.__print_accounts(
					[(options.user, bool(a['pass']))], d)
			return
		elif options.list:
			self.__print_accounts(base.config.get_account_names(),d)
			return

		if len(args) == 1:
			if args[0] == "set":
				self.__set_options(options, base)
			elif args[0] == "unset":
				self.__unset_options(options, base)
			else:
				base.parser.error("Unknown directive: %s" %
						args[0])
			return
		else:
			base.parser.error("Insufficient options specified")


class NameCacheCommand(Command):
	def __str__(self):
		return "namecache"

	def get_options(self):
		self.options.append(make_option("-c", "--clear",
			default=False, action="store_true",
			help="Clear the name cache."))
		self.options.append(make_option("-@", "--at",
			default=False, action="store_true",
			help="Prepend usernames with '@'."))
		return self.options

	def get_usage(self):
		return "%prog [options]"

	def get_help(self):
		return "Access and manipulate the username cache."

	def do_command(self, options, args, base):
		if options.clear:
			base.config.name_cache_clear()
			return

		if options.at:
			for n in base.config.name_cache_list():
				base.output("@%s" % n)
		else:
			for n in base.config.name_cache_list():
				base.output(n)

class IPLimitCommand(Command):
	def __str__(self):
		return "iplimit"

	def get_usage(self):
		return "%prog [options]"

	def get_help(self):
		return "Show the API rate limit for your IP address."

	def do_command(self, options, args, base):

		answer = base.twitter.account_rate_limit_status(False)

		if answer:
			try:
				rl = data.RateLimit(answer)
				base.output(str(rl)[:-1])
			except Exception, e:
				base.parser.error(e)


class AccountLimitCommand(CommandWithAuth):
	def __str__(self):
		return "accountlimit"

	def get_usage(self):
		return "%prog [options]"

	def get_help(self):
		return "Show the API rate limit for your Twitter account."

	def do_command(self, options, args, base):
		user, pwd = self.get_auth(options, base)
		answer = self.callwithauth(base.twitter.account_rate_limit_status,
				options, [True], base, user, pwd)

		if answer:
			try:
				rl = data.RateLimit(answer)
				base.output(str(rl)[:-1])
			except Exception, e:
				base.parser.error(e)

