# Copyright (c) 2009 Andrew Price
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Provides caching, access and modification methods to a JSON-formatted config
file.
"""

import os
import simplejson
from stat import *

class Config:
	def __init__(self):
		"""
		Initialises a Config object and parses the config file.
		"""
		self.dirty = False
		homedir = os.environ['HOME']
		self.path = os.path.join(homedir, '.twytrc.json')
		self.config = {'accounts':{}, 'stamps':{}, 'default':"", 'names':[]}
		self.refresh_config()
	
	def refresh_config(self):
		"""
		Parses the config file into self.accounts. If the config file
		doesn't exist, it is created.
		"""
		if not os.path.exists(self.path):
			self.dirty = True
		else:
			fd = open(self.path, "r")
			self.config = simplejson.load(fd)
			fd.close()

		# Check sanity
		if not self.config.has_key('stamps'):
			self.config['stamps'] = {}
			self.dirty = True

		if not self.config.has_key('accounts'):
			self.config['accounts'] = {}
			self.dirty = True

		if not self.config.has_key('default'):
			self.config['default'] = ""
			self.dirty = True

		if not self.config.has_key('names'):
			self.config['names'] = []
			self.dirty = True

		# Backwards compat code
		if type(self.config['names']) == dict:
			self.config['names'] = self.config['names'].keys()
			self.dirty = True
	
	def add_account(self, name, passwd=""):
		"""
		Adds a new account only if the name doesn't already exist.
		Returns True if added, False otherwise.
		"""
		if (not name) or self.config['accounts'].has_key(name):
			return False

		self.config['accounts'][name] = {'pass':passwd}
		if len(self.config['accounts']) == 1:
			self.set_default_account(name)
		self.dirty = True
		return True
	
	def remove_account(self, name):
		"""
		Remove an account from the config if it's stored.
		"""
		if self.have_account(name):
			del(self.config['accounts'][name])
			self.dirty = True
			if self.config['default'] == name:
				self.config['default'] = ""

	def get_account(self, name):
		"""
		Returns a dict containing the details for the account which
		matches name in the config. Returns empty dict if not found.
		"""
		try:
			return self.config['accounts'][name]
		except KeyError:
			return {}

	def get_account_names(self):
		"""
		Returns all account names and whether they have a password
		as a list of account, bool pairs.
		"""
		accounts = []
		for i in self.config['accounts'].keys():
			accounts.append((i,
				bool(self.config['accounts'][i]['pass'])))
		return accounts

	def get_default_account(self):
		"""
		Returns the name of the default account or "".
		"""
		return self.config['default'] or ""

	def set_default_account(self, name):
		"""
		Sets the default account or, if name isn't already stored,
		does nothing.
		"""
		if self.config['accounts'].has_key(name) and \
		   self.config['default'] != name:
			self.config['default'] = name
			self.dirty = True

	def set_password(self, name, password):
		"""
		Sets the password for the named account. Creates the account
		name entry if it doesn't exist.
		"""
		if not name:
			return
		if not self.get_account(name):
			self.add_account(name, password)
		elif password != self.get_password(name):
			self.set_account_value(name, 'pass', password)

	def get_password(self, name):
		"""
		Returns the password for the named account or "".
		"""
		try:
			return self.config['accounts'][name]['pass']
		except KeyError:
			return ""

	def set_account_value(self, name, field, value):
		"""
		Sets the value of a field for the account name specified.
		Returns True if the value was set and False on failure (i.e. the
		named account doesn't exist in the config).
		"""
		try:
			self.config['accounts'][name][field] = value
			self.dirty = True
			return True
		except KeyError:
			return False

	def have_account(self, name):
		"""
		Returns True if the named account is stored in the config or
		False otherwise.
		"""
		if not name:
			return False
		try:
			return bool(self.config['accounts'][name])
		except KeyError:
			return False

	def set_stamp(self, timeline, user, stamp):
		"""
		Sets the stamp for a user's timeline.
		"""
		if not self.config['stamps'].has_key(str(timeline)):
			self.config['stamps'][str(timeline)] = {}
		self.config['stamps'][str(timeline)][user] = stamp
		self.dirty = True
	
	def get_stamp(self, timeline, user):
		"""
		Returns the stamp for a user's timeline.
		"""
		try:
			return self.config['stamps'][str(timeline)][user]
		except KeyError:
			return None
	
	def name_cache_add(self, name):
		"""
		Adds a name to the name cache.
		"""
		if name not in self.config['names']:
			self.config['names'].append(name)

	def name_cache_clear(self):
		"""
		Clears the name cache.
		"""
		if len(self.config['names']) > 0:
			del self.config['names'][:]
			self.dirty = True
	
	def name_cache_list(self):
		"""
		Returns a list of all names in the name cache.
		"""
		return self.config['names']

	def dirty_writeback(self):
		"""
		Writes the configuration back to the config file if dirty.
		This function ensures that the mode of the config file is 0600.
		"""
		if not self.dirty:
			return

		f = open(self.path, "w")
		simplejson.dump(self.config or {}, f, indent=1)
		f.close()

		s = os.stat(self.path)
		if S_IMODE(s[0]) != (S_IRUSR|S_IWUSR):
			os.chmod(self.path, S_IRUSR|S_IWUSR)
		self.dirty = False
