#!/usr/bin/env python
import sys
import os
import twyt.info as info
from distutils.core import setup

version_string = info.VERSION

setup(name="twyt",
	version=version_string,
	description="A command line Twitter client and Twitter library",
	author="Andrew Price",
	author_email="andy@andrewprice.me.uk",
	url="http://andrewprice.me.uk/projects/twyt",
	packages = ['twyt'],
	package_dir = {'twyt': 'twyt'},
	scripts = ['scripts/twyt'],
	data_files = [
		('share/man/man1', ['docs/twyt.1'])
		]
	)
